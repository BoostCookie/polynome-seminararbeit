.PHONY: clean show default
name=arbeit
refs=quellen.bib

default: build/$(name).pdf

build/$(name).pdf: src/$(refs) src/$(name).tex src/python/polynomials.py
	mkdir -p build/python
	cp src/$(refs) build/
	cp src/python/polynomials.py build/python/
	latexmk -pdf -output-directory=build src/$(name).tex

clean:
	rm -rf build
	find src/ -maxdepth 1 ! -name src ! -name $(name).tex ! -name $(refs) ! -name python -delete

show: build/$(name).pdf
	xdg-open build/$(name).pdf

