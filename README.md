Meine ausgebesserte Seminararbeit in Mathematik. Es wird beschrieben wie man Polynome bis zum Grad 4 löst und auch angeschnitten, warum es keine Lösungsformel für Polynome vom Grad 5 oder höher geben kann.

Hier ist die kompilierte PDF: [https://gitlab.com/BoostCookie/polynome-seminararbeit/-/jobs/artifacts/master/browse?job=compile_pdf](https://gitlab.com/BoostCookie/polynome-seminararbeit/-/jobs/artifacts/master/browse?job=compile_pdf).
