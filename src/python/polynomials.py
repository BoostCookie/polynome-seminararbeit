#!/usr/bin/env python3

import math
import cmath
import re

zeta = complex(-0.5, 0.5 * cmath.sqrt(3))
zeta2 = zeta**2

# for x + p = 0
def linear(p):
    yield -p

# for x^2 + px + q = 0
# x = -p/2 +- sqrt((p/2)^2 - 1)
def quadraticPQ(p, q):
    sqrt = cmath.sqrt(0.25 * p**2 - q)
    p2 = -0.5 * p
    yield p2 + sqrt
    yield p2 - sqrt

# for x^3 = c
def cubic_trivial(c):
    cbrt = c**(1 / 3)
    yield cbrt
    yield zeta * cbrt
    yield zeta2 * cbrt

# for x^3 + px + q = 0
def cubicPQ(p, q):
    u3, v3 = list(quadraticPQ(q, -(p / 3)**3))
    minus_p_phase = cmath.phase(-p)
    u = next(cubic_trivial(u3))
    v_iter = cubic_trivial(v3)
    v = next(v_iter)
    calc_res = lambda arg : abs(cmath.phase(u * arg) - minus_p_phase)
    prev_result = calc_res(v)
    for vval in v_iter:
        new_result = calc_res(vval)
        if new_result < prev_result:
            v = vval
            prev_result = new_result
    yield u + v
    yield zeta * u + zeta2 * v
    yield zeta2 * u + zeta * v

# for x^3 + ax^2 + bx + c = 0
def cubicABC(a, b, c):
    a3 = a / 3
    for y in cubicPQ(-a**2 / 3 + b, 2 * a3**3 - a * b / 3 + c):
        yield y - a3

# for x^4 + px^2 + qx + r = 0
def quarticPQR(p, q, r):
    p2 = 0.5 * p
    z_iter = cubicABC(-p2, -r, p2 * r - 0.125 * q**2)
    z = next(z_iter)
    calc_res = lambda zval : abs(2 * cmath.sqrt(2 * zval - p) + cmath.sqrt(zval**2 - r) - q)
    prev_result = calc_res(z)
    for zval in z_iter:
        new_result = calc_res(zval)
        if new_result < prev_result:
            z = zval
            prev_result = new_result
    zpsqrt = cmath.sqrt(2 * z - p)
    zrsqrt = cmath.sqrt(z**2 - r)
    for solution in quadraticPQ(-zpsqrt, z + zrsqrt):
        yield solution
    for solution in quadraticPQ( zpsqrt, z - zrsqrt):
        yield solution

#for x^4 + ax^3 + bx^2 + cx + d = 0
def quarticABCD(a, b, c, d):
    a4 = 0.25 * a
    a4sqr = a4**2
    a4cbd = a4sqr * a4
    for y in quarticPQR(-6 * a4sqr + b, 8 * a4cbd - 2 * a4 * b + c, a4sqr * (-3 * a4sqr + b) - a4 * c + d):
        yield y - a4

def round_complex(number, digits=0):
    return complex(round(number.real, digits), round(number.imag, digits))

def complex_str(number):
    if number.imag == 0:
        return str(number.real)
    else:
        return str(number)[1:-1]

re_brace = re.compile(r"\([^)]*\)")
re_summand = re.compile(r"[+-]?(\([+-]?)?\d*(\.\d*)?([+-]\d*(\.\d*)?j)?\)?(x(\^\d+)?)?")
def read_polystr(input_str):
    order_coeff_dict = {0: 0}
    input_str = input_str.replace("i", "j").replace(" ", "")
    while input_str:
        match = re_summand.match(input_str)
        if not match:
            break
        group = match.group()

        order = 1
        coeff = complex(1, 0)

        pre, post = "", ""
        if "x" in group:
            pre, post = group.split("x")
        else:
            order = 0
            pre = group
        input_str = input_str[match.end():]

        if pre:
            if pre.startswith("-"):
                coeff = -1
            search_brace = re_brace.search(pre)
            if search_brace:
                coeff *= complex(search_brace.group()[1:-1])
            else:
                if pre[-1].isdigit():
                    coeff = complex(pre)

        if "^" in post:
            order = int(post[1:])

        if order in order_coeff_dict:
            order_coeff_dict[order] += coeff
        else:
            order_coeff_dict[order] = coeff

    return order_coeff_dict

def main():
    #run until user wants to stop
    while True:
        print("Bitte geben Sie ein Polynom (z. B. x^3 - (2+3j)x^2 + 6x - 10) ein.")
        order_coeff_dict = read_polystr(input())

        max_order = max(order_coeff_dict.keys())
        if max_order > 4:
            print("Bitte wählen Sie ein Polynom vom Grad 4 oder kleiner.")
            next

        highest_coeff = order_coeff_dict[max_order]
        if highest_coeff != 1:
            for order, coeff in order_coeff_dict.items():
                order_coeff_dict[order] /= highest_coeff

        for order in range(max_order):
            if not (order in order_coeff_dict):
                order_coeff_dict[order] = complex(0)

        args = [order_coeff_dict[i] for i in range(max_order - 1, -1, -1)]
        fun = linear
        if max_order == 4:
            fun = quarticABCD
        elif max_order == 3:
            fun = cubicABC
        elif max_order == 2:
            fun = quadraticPQ

        print("Nullstellen:")
        for solution in fun(*args):
            print(complex_str(round_complex(solution, 10)))

        print("Wiederholen? (J/n)")
        if "n" in input().lower():
            break

if __name__ == "__main__":
    main()

